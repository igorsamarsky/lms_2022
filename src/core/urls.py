from django.urls import path

from core.views import IndexView, UserLoginView, UserLogoutView, UserRegistrationView, ActivateUser, UserProfileView

appname = "core"


urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("login/", UserLoginView.as_view(), name="login"),
    path("logout/", UserLogoutView.as_view(), name="logout"),
    path("registration/", UserRegistrationView.as_view(), name="registration"),
    path("profile/<int:pk>", UserProfileView.as_view(), name="user_profile"),
    path("activate/<str:uuid64>/<str:token>/", ActivateUser.as_view(), name="activate_user"),
]
