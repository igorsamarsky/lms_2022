from django.db.models import Q


def format_records(records: list) -> str:
    return "<br>".join(str(record) for record in records)


def objects_search(req, model, search_fields):
    for field_name, field_value in req.items():
        if field_name == "csrfmiddlewaretoken":
            continue

        if field_name == "search_text":
            or_filter = Q()
            for field in search_fields:
                # accumulate filter condition
                or_filter |= Q(**{f"{field}__icontains": field_value})

            return model.objects.filter(or_filter)
        else:
            if field_value:
                return model.objects.filter(**{field_name: field_value})

    return model.objects.all()
