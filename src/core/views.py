from django.contrib.auth import login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import TemplateView, CreateView, RedirectView, DetailView

from core.forms import UserRegistrationForm
from core.models import Customer
from core.services.emails import send_registration_email
from core.utils.token_generator import TokenGenerator


class IndexView(TemplateView):
    template_name = "core/index.html"
    http_method_names = ["get"]


class NotFoundView(TemplateView):
    template_name = "core/404.html"


class UserLoginView(LoginView):
    ...


class UserLogoutView(LogoutView):
    ...


class UserRegistrationView(CreateView):
    template_name = "registration/registration.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(self.request, self.object)

        return super().form_valid(form)


class ActivateUser(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (ValueError, get_user_model().DoesNotExist, TypeError):
            return HttpResponse("Wrong data!!!")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)

            return super().get(request, *args, **kwargs)

        return HttpResponse("Wrong data!!!")


class UserProfileView(LoginRequiredMixin, DetailView):
    template_name = "core/user_profile.html"
    pk_url_kwarg = "pk"
    queryset = Customer.objects.all()
