import random
from uuid import uuid4

from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker

from core.utils.validators import first_name_validator, gpa_validator


class Person(models.Model):
    uuid = models.UUIDField(primary_key=True, editable=False, default=uuid4, unique=True, db_index=True)
    avatar = models.ImageField(upload_to="avatars/", null=True)
    resume = models.FileField(upload_to="resumes/", null=True)
    first_name = models.CharField(max_length=120, null=True, validators=[MinLengthValidator(2), first_name_validator])
    last_name = models.CharField(max_length=120, null=True, validators=[MinLengthValidator(2)])
    email = models.EmailField(max_length=120, null=True)
    birth_day = models.DateField(null=True)
    department = models.CharField(max_length=120, null=True)

    class Meta:
        abstract = True


class Student(Person):
    avatar = models.ImageField(upload_to="students_avatars/", null=True)
    resume = models.FileField(upload_to="students_resumes/", null=True)
    gpa = models.FloatField(default=0, null=True, validators=[gpa_validator])
    group = models.ForeignKey(to="groups.Group", on_delete=models.CASCADE, related_name="students")

    def __str__(self):
        return f"{self.first_name}_{self.last_name} ({self.pk})"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                gpa=round(random.uniform(1, 5), 2),
                birth_day=faker.date_time_between(start_date="-30y", end_date="-18y"),
                department=faker.job(),
                group=faker.job(),
            )
