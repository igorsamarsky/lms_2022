from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, ListView

from core.utils.helpers import objects_search
from students.forms import StudentForm
from students.models import Student


class GetStudentsView(LoginRequiredMixin, ListView):
    template_name = "students/students_list.html"
    model = Student

    def get_students(self):
        search_fields = ["last_name", "first_name", "email"]
        return objects_search(self.request.GET, self.model, search_fields)

    def get_queryset(self):
        return self.get_students()


class CreateStudentView(LoginRequiredMixin, CreateView):
    template_name = "students/create_student.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:students_list")


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    template_name = "students/update_student.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:students_list")
    pk_url_kwarg = "uuid"
    queryset = Student.objects.all()


class DeleteStudentView(LoginRequiredMixin, DeleteView):
    template_name = "students/delete_student.html"
    model = Student
    success_url = reverse_lazy("students:students_list")
    pk_url_kwarg = "uuid"
    queryset = Student.objects.all()
