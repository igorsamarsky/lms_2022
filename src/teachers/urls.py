from django.urls import path

from teachers.views import CreateTeacherView, UpdateTeacherView, DeleteTeacherView, GetTeachersView

app_name = "teachers"

urlpatterns = [
    path("", GetTeachersView.as_view(), name="teachers_list"),
    path("create/", CreateTeacherView.as_view(), name="create_teacher"),
    path("update/<uuid:uuid>", UpdateTeacherView.as_view(), name="update_teacher"),
    path("delete/<uuid:uuid>", DeleteTeacherView.as_view(), name="delete_teacher"),
]
