import random

from django.db import models
from faker import Faker

from students.models import Person


class Teacher(Person):
    avatar = models.ImageField(upload_to="teachers_avatars/", null=True)
    resume = models.FileField(upload_to="teachers_resumes/", null=True)
    salary = models.IntegerField(default=0, null=True)
    groups = models.ManyToManyField(to="groups.Group", related_name="teachers")

    def __str__(self):
        return f"{self.first_name}_{self.last_name} ({self.pk})"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                salary=random.randint(500, 2500),
                birth_day=faker.date_time_between(start_date="-30y", end_date="-18y"),
                department=faker.job(),
                group=faker.job(),
            )
