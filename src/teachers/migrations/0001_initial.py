# Generated by Django 3.2.16 on 2023-02-07 21:21

import core.utils.validators
import django.core.validators
from django.db import migrations, models
import uuid


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("groups", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Teacher",
            fields=[
                (
                    "uuid",
                    models.UUIDField(
                        db_index=True,
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                        unique=True,
                    ),
                ),
                (
                    "first_name",
                    models.CharField(
                        max_length=120,
                        null=True,
                        validators=[
                            django.core.validators.MinLengthValidator(2),
                            core.utils.validators.first_name_validator,
                        ],
                    ),
                ),
                (
                    "last_name",
                    models.CharField(
                        max_length=120, null=True, validators=[django.core.validators.MinLengthValidator(2)]
                    ),
                ),
                ("email", models.EmailField(max_length=120, null=True)),
                ("birth_day", models.DateField(null=True)),
                ("department", models.CharField(max_length=120, null=True)),
                ("avatar", models.ImageField(null=True, upload_to="teachers_avatars/")),
                ("resume", models.FileField(null=True, upload_to="teachers_resumes/")),
                ("salary", models.IntegerField(default=0, null=True)),
                ("groups", models.ManyToManyField(related_name="teachers", to="groups.Group")),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
