from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, ListView

from core.utils.helpers import objects_search
from teachers.forms import TeacherForm
from teachers.models import Teacher


class GetTeachersView(LoginRequiredMixin, ListView):
    template_name = "teachers/teachers_list.html"
    model = Teacher

    def get_teachers(self):
        search_fields = ["last_name", "first_name", "email"]
        return objects_search(self.request.GET, self.model, search_fields)

    def get_queryset(self):
        return self.get_teachers()


class CreateTeacherView(LoginRequiredMixin, CreateView):
    template_name = "teachers/create_teacher.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:teachers_list")


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    template_name = "teachers/update_teacher.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:teachers_list")
    pk_url_kwarg = "uuid"
    queryset = Teacher.objects.all()


class DeleteTeacherView(LoginRequiredMixin, DeleteView):
    template_name = "teachers/delete_teacher.html"
    model = Teacher
    success_url = reverse_lazy("teachers:teachers_list")
    pk_url_kwarg = "uuid"
    queryset = Teacher.objects.all()
