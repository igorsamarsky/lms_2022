from django.contrib import admin  # NOQA
from django.urls import path

from groups.views import CreateGroupView, UpdateGroupView, DeleteGroupView, GetGroupsView, GetGroupInfoView

app_name = "groups"

urlpatterns = [
    path("", GetGroupsView.as_view(), name="groups_list"),
    path("create/", CreateGroupView.as_view(), name="create_group"),
    path("update/<uuid:uuid>", UpdateGroupView.as_view(), name="update_group"),
    path("delete/<uuid:uuid>", DeleteGroupView.as_view(), name="delete_group"),
    path("info/<uuid:uuid>", GetGroupInfoView.as_view(), name="group_info"),
]
