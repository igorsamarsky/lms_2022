from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, ListView, DetailView

from core.utils.helpers import objects_search
from groups.forms import GroupForm
from groups.models import Group


class GetGroupInfoView(LoginRequiredMixin, DetailView):
    template_name = "groups/group_info.html"
    pk_url_kwarg = "uuid"
    queryset = Group.objects.all()


class GetGroupsView(LoginRequiredMixin, ListView):
    template_name = "groups/groups_list.html"
    model = Group

    def get_groups(self):
        search_fields = ["title"]
        return objects_search(self.request.GET, self.model, search_fields)

    def get_queryset(self):
        return self.get_groups()


class CreateGroupView(LoginRequiredMixin, CreateView):
    template_name = "groups/create_group.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:groups_list")


class UpdateGroupView(LoginRequiredMixin, UpdateView):
    template_name = "groups/update_group.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:groups_list")
    pk_url_kwarg = "uuid"
    queryset = Group.objects.all()


class DeleteGroupView(LoginRequiredMixin, DeleteView):
    template_name = "groups/delete_group.html"
    model = Group
    success_url = reverse_lazy("groups:groups_list")
    pk_url_kwarg = "uuid"
    queryset = Group.objects.all()
