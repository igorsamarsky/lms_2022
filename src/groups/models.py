import random
from uuid import uuid4

from django.db import models
from faker import Faker

from core.utils.validators import gpa_validator, median_age_validator


class Group(models.Model):
    uuid = models.UUIDField(primary_key=True, editable=False, default=uuid4, unique=True, db_index=True)
    title = models.CharField(max_length=120, null=True)
    department = models.CharField(max_length=120, null=True)
    students_count = models.PositiveIntegerField(default=0, null=True)
    gpa = models.FloatField(default=0, null=True, validators=[gpa_validator])
    median_age = models.FloatField(default=0, null=True, validators=[median_age_validator])
    start_date = models.DateField(null=True)

    def __str__(self):
        return f"{self.title} [{self.department}] ({self.pk})"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            department = faker.job()
            start_date = faker.date_time_between(start_date="-4y")
            start_year = start_date[4]
            cls.objects.create(
                title=f"{department[0]}-{start_year}",
                department=department,
                students_count=faker.random.randint(1, 50),
                gpa=round(random.uniform(1, 5), 2),
                median_age=round(random.uniform(18, 30), 2),
                start_date=start_date,
            )
